<?php

namespace app\models;

use yii\web\UploadedFile;
use yii\base\Model;
use vova07\fileapi\behaviors\UploadBehavior;
use Yii;

/**
 * UploadForm is the model behind the upload form.
 */
class UploadForm extends Model
{
    /**
     * @var UploadedFile|Null file attribute
     */
    public $picture;

    public function behaviors()
    {
        return [
            'uploadBehavior' => [
                'class' => UploadBehavior::className(),
                'attributes' => [
                    'picture' => [
                        'path' => '@app/web/uploads',
                        'tempPath' => '@app/web/assets/tmp',
                        'url' => '/uploads'
                    ]
                ]
            ]
        ];
    }
}