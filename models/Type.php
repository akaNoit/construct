<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "type".
 *
 * @property integer $id
 * @property integer $id_products
 * @property string $name
 * @property string $path_to_picture
 * @property integer $is_active
 *
 * @property Products $idProducts
 */
class Type extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_products', 'is_active'], 'integer'],
            [['name', 'path_to_picture'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_products' => 'Id Products',
            'name' => 'Name',
            'path_to_picture' => 'Path To Picture',
            'is_active' => 'Is Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProducts()
    {
        return $this->hasOne(Products::className(), ['id' => 'id_products']);
    }
}
