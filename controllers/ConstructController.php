<?php

namespace app\controllers;

use app\models\Category;
use app\models\Products;
use app\models\Type;
use app\models\UploadForm;
use yii\web\UploadedFile;
use vova07\fileapi\actions\UploadAction as FileAPIUpload;
use Yii;

class ConstructController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'fileapi-upload' => [
                'class' => FileAPIUpload::className(),
                'path' => '@app/web/uploads/'
            ]
        ];
    }

    public function actionIndex()
    {
        $data = Category::find()
            ->all();
        return $this->render('index',[
            'data' => $data,
        ]);
    }

    public function actionProduct($id)
    {
        $data = Products::find()
            ->where(['id_category' => $id])
            ->all();
        return $this->render('product',[
            'data' => $data,
        ]);
    }

    public function actionType($id)
    {
        $type = Type::find()
            ->where(['id_product' => $id])
            ->all();

        if(!empty($type)) {

            return $this->render('type',[
                'data' => $type,
            ]);
        } else {

            $upload = new UploadForm();
            $data ['type'] = $type;
            $data ['upload'] = $upload;

            return $this->render('picture',[
                'data' => $data,
            ]);
        }
    }

    public function actionPicture($id)
    {
        $type = Type::find()
            ->where(['id' => $id])
            ->all();
        $upload = new UploadForm();
        $data ['type'] = $type;
        $data ['upload'] = $upload;

        return $this->render('picture',[
            'data' => $data,
        ]);
    }

}
