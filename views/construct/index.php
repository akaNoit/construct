<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
?>
<?php Url::remember();?>

<h1>Выберите изделие</h1>
<div class="blocks">
    <?php foreach ($data as $post): ?>
        <div class="object">
            <?= Html::a($post->name, array('construct/product', 'id'=>$post->id)); ?>
        </div>
    <?php endforeach; ?>
</div>
