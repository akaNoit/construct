<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
?>

<h1><?= Html::a('<', Url::previous()); ?><?= Yii::$app->controller->action->id; ?></h1>
<?php Url::remember();?>

<div class="blocks">
    <?php foreach ($data as $post): ?>
        <div class="object">
            <?= Html::a($post->name, array('construct/type', 'id'=>$post->id)); ?>
        </div>
    <?php endforeach; ?>
</div>
