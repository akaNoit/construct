<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use vova07\fileapi\Widget as FileAPI;

/* @var $this yii\web\View */
?>

<h1><?= Html::a('<', Url::previous()); ?>Выберите источник</h1>

<?php $form = ActiveForm::begin([
    'options' => ['accept' => 'image/*', 'enctype' => 'multipart/form-data']
]); ?>

<?= $form->field($model, 'file')->fileInput() ?>

<button>Submit</button>

<?php ActiveForm::end(); ?>
