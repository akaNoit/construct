<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use vova07\fileapi\Widget as FileAPI;

/* @var $this yii\web\View */
?>


<h3><?= Html::a('<', Url::previous()); ?>Выберите источник</h3>
<div class="phone-form">
    <?php $form = ActiveForm::begin(
        [
            'options' => [
                'class' => 'center'
            ]
        ]
    ); ?>

    <div class="upload-button">
        <?= $form->field($data['upload'], 'picture')->widget(
            FileAPI::className(),
            [
                'settings' => [
                    'url' => ['fileapi-upload']
                ],
                'crop' => true,
                'cropResizeWidth' => 400,
            ]
            )->label(false); ?>
    </div>

    <div class="phone-object">
        <?= Html::img(Url::to('@web/uploads/type/') . $data['type'][0]->path_to_picture); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
